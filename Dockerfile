ARG BASE_IMAGE="alpine:3.15"
FROM $BASE_IMAGE as buildenv

RUN apk add --no-cache autoconf automake bison flex g++ git glu-dev \
        gtk+2.0-dev libxml2-dev libxslt-dev make mesa-dev openssl-dev \
        py2-setuptools python2-dev && \
    wget  https://bootstrap.pypa.io/pip/2.7/get-pip.py && \
    python get-pip.py

RUN mkdir /wxpython && \
    wget https://downloads.sourceforge.net/project/wxpython/wxPython/3.0.2.0/wxPython-src-3.0.2.0.tar.bz2 -O - | tar xjf - -C /wxpython --strip-components=1 && \
    cd /wxpython && \
    ./autogen.sh && \
    ./configure --with-opengl && \
    make  -j$(nproc) install

RUN cd /wxpython/wxPython && python setup.py install --prefix=/usr/local
RUN mkdir /dist && cd /dist && \
    pip wheel -w . future zeroconf==0.19.1 numpy==1.16.5 matplotlib==2.0.2 lxml pyro sslpsk pyserial

RUN git clone --depth=1 https://github.com/kinsamanka/OpenPLC_Editor /editor && \
    cd /editor/matiec && \
    autoreconf -i && \
    ./configure && \
    make -s && \
    chmod a+x ../editor/Beremiz.py

FROM $BASE_IMAGE

ARG BUILD_DATE
ARG GIT_REV
ARG DOCKER_TAG="0.1"

LABEL \
    org.opencontainers.image.title="alpine-openplc-editor" \
    org.opencontainers.image.description="OpenPLC Editor running on Alpine Linux" \
    org.opencontainers.image.authors="GP Orcullo<kinsamanka@gmail.com>" \
    org.opencontainers.image.version=$DOCKER_TAG \
    org.opencontainers.image.url="https://hub.docker.com/repository/docker/kinsamanka/alpine-openplc-editor" \
    org.opencontainers.image.source="https://gitlab.com/kinsa13/tango/Docker/alpine-openplc-editor" \
    org.opencontainers.image.revision=$GIT_REV \
    org.opencontainers.image.created=$BUILD_DATE


COPY --from=buildenv /usr/local/lib /usr/local/lib
COPY --from=buildenv /editor/editor /opt/openplc/editor
COPY --from=buildenv /editor/matiec/lib /opt/openplc/matiec/lib
COPY --from=buildenv /editor/matiec/iec2c /usr/local/bin
COPY --from=buildenv /get-pip.py /tmp
COPY --from=buildenv /dist /tmp/dist

RUN apk add --no-cache dbus-x11 g++ glu gtk+2.0 libxml2 libxslt mesa openssl py-cairo \
        py-netifaces py-pip python2 py3-xdg terminus-font xhost xpra xpra-webclient && \
    rm -f /var/cache/apk/* && \
    pip install pyinotify && \
    python2 /tmp/get-pip.py && \
    pip2 install /tmp/dist/*whl && \
    cp -a /usr/local/lib/python2.7/site-packages/* /usr/lib/python2.7/site-packages/ && \
    rm -rf /tmp/* && \
    adduser --disabled-password --gecos "openplc_user" --uid 1000 user && \
    mkdir -p /run/user/1000/xpra && \
    mkdir -p /run/xpra && \
    chown user:user /run/user/1000/xpra && \
    chown user:user /run/xpra

WORKDIR /home/user
USER user
EXPOSE 8000
VOLUME /home/user

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8
ENV LC_ALL en_US.UTF-8

CMD rm -rf /tmp/.X* &&  xpra start --start=/opt/openplc/editor/Beremiz.py --bind-tcp=0.0.0.0:8000 --daemon=no --xvfb="/usr/bin/Xvfb +extension Composite -screen 0 1920x1080x24+32 -nolisten tcp -noreset"  --pulseaudio=no --notifications=no --bell=no --mdns=no --webcam=no --pulseaudio=no :100
